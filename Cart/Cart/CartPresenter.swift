//
//  CartPresenter.swift
//  Cart
//
//  Created by Alejandro Jiménez Agudo on 20/11/15.
//  Copyright © 2015 Gigigo SL. All rights reserved.
//

import Foundation


protocol CartView {
	func showProducts(products: [Product])
	func showTotalAmount(amount: Double)
}

class CartPresenter {
	
	var view: CartView!

	private let cartInteractor = CartInteractor()
	
	func viewDidLoad() {
		let products = self.cartInteractor.productsInCart()
		let totalAmount = self.cartInteractor.totalAmount()
		self.view.showProducts(products)
		self.view.showTotalAmount(totalAmount)
	}
	
	func shouldShowQuantity(product: Product) -> Int {
		return self.cartInteractor.productQuantity(product)
	}
	
}
