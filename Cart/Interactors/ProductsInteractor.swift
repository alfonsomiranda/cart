//
//  ProductsInteractor.swift
//  Cart
//
//  Created by Alejandro Jiménez Agudo on 13/11/15.
//  Copyright © 2015 Gigigo SL. All rights reserved.
//

import Foundation


class ProductsInteractor {
	
	private var dataManager = ProductsDataManager()
	
	func productList() -> [Product] {
		let products = self.dataManager.fetchProducts()
		return products
	}
	
}